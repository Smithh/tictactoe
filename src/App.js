import React, { Component } from 'react';
import { EventEmitter } from 'events';
import './styles/App.css';
import Menu from './components/menu';
import Game from './components/game';

class App extends Component {

  constructor(){
    super();
    this.state = {
      componentIndex : 1,
      gameType : ""
    }
  }

  componentWillMount(){
    this.eventEmitter = new EventEmitter();

    this.eventEmitter.on("ActiveScreen", (newIndexValue,gameType) => {
      this.updateActiveComponent({newActiveComponent : newIndexValue, gameType: gameType});
    });

    this.eventEmitter.on("MainMenu", (newIndexValue) => {
      this.updateActiveComponent({newActiveComponent: newIndexValue, gameType : ""})
    })
  }

  updateActiveComponent({newActiveComponent,gameType}){
    this.setState({componentIndex : newActiveComponent, gameType : gameType})
  }

  render() {
    let activeComponent;
    this.state.componentIndex === 1 ? activeComponent = <Menu eventEmitter = {this.eventEmitter}/> : activeComponent = <Game eventEmitter = {this.eventEmitter} gameType = {this.state.gameType}/>

    return (
      <div className="App">
        <div className="title">
          <span className="title-text">Tic_Tac_Toe</span>
        </div>
        {activeComponent}
      </div>
    );
  }
}

export default App;
