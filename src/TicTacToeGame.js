class TicTacToeGame {

    // TICTACTOE GAME IN JS
    /**
     * 
     * @param gameType describes the opponent type ["Player_two","Easy_CPU","Normal_CPU"]
     * @param startingPlayer default should be X, if player chooses rematch then first player can be O
     * @param score in a form of [number,number,number], first element is playerOne score, second element is playerTwo score, third is the number of draws
     */
    constructor(gameType,startingPlayer,score){
        this.gameType = gameType;
        this.currentStartingPlayer = startingPlayer;
        this.gameField = ["","","","","","","","",""];
        this.winningMoves = [[0,1,2],[3,4,5],[6,7,8],[0,3,6],[1,4,7],[2,5,8],[0,4,8],[2,4,6]]
        this.playerOne = "X";
        this.playerTwo = "O";
        this.currentTurn = startingPlayer;
        this.score = score;
        this.winner = "";
        //If bot has to make first move, make it
        if((gameType === "Easy_CPU" || gameType === "Normal_CPU") && startingPlayer === this.playerTwo){
            let move;
            if(gameType === "Easy_CPU"){
               move = this.easyBotMove();
            }
            else{
                move = this.normalBotMove();
            }
            this.gameField[move] = this.currentTurn;
            this.currentTurn === this.playerOne ? this.currentTurn = this.playerTwo : this.currentTurn = this.playerOne; 
        }
    };

    /**
     * Check if cell belongs to player one
     * @param element cell coord that we check to be player one cell 
     * @param field field that we check cell owner on
     * @return boolean type 
     */
    isPlayerOneCell(element,field){
        return field[element] === this.playerOne;
    }

    /**
     * Check if cell belongs to player one
     * @param element cell coord that we check to be player one cell 
     * @param field field that we check cell owner on
     * @return boolean type 
     */
    isPlayerTwoCell(element,field){
        return field[element] === this.playerTwo;
    }

    /**
     * checks if game was found and then updates the score if game was won
     * @return true if game was won
     */
    checkWinner(){
        let res = false;
        this.winningMoves.forEach((vector) => {
            if(vector.every((elem) => this.isPlayerOneCell(elem,this.gameField)) || vector.every((elem) => this.isPlayerTwoCell(elem,this.gameField))){
                if(this.gameField[vector[0]] === this.playerOne){
                    this.winner = "Player_One";
                    this.score = [this.score[0]+1, this.score[1], this.score[2]];
                } 
                else{
                    this.winner = this.gameType;
                    this.score = [this.score[0], this.score[1] + 1, this.score[2]];
                }
                res = true;
            }
        });
        return res;
    }

    /**
     * If all fields in gameFields are full, return true, else False
     * @return boolean
     */
    checkFieldFull(){
        let res = this.gameField.every((element) => {
            return element !== "";
        })
        if(res){
            this.score = [this.score[0], this.score[1], this.score[2]+1];
            this.winner = "Draw"
        }
        return res;
    }

    /**
     * Takes in coordinate where next move is going to be placed, if coordinate is already taken, no move is made.
     * Makes bot move if player two is bot
     * @param moveCoord coordinate where the next move is placed 
     * @return returns true if game is over, false if game can continue
     */
    addMove(moveCoord){
        if(this.gameField[moveCoord] === ""){
            this.gameField[moveCoord] = this.currentTurn;
            this.currentTurn === this.playerOne ? this.currentTurn = this.playerTwo : this.currentTurn = this.playerOne;
            if(this.checkGameState()){
                return true;
            }
            if(this.gameType !== "Player_two"){
                let move;
                if(this.gameType === "Easy_CPU"){
                    move = this.easyBotMove();
                }
                else{
                    move = this.normalBotMove();
                }
                this.gameField[move] = this.currentTurn;
                this.currentTurn === this.playerOne ? this.currentTurn = this.playerTwo : this.currentTurn = this.playerOne;
                if(this.checkGameState()){
                    return true;
                }
            }
            return false;
        }
    }

    /**
     * checks if game ended
     * @return returns true if game ended
     */
    checkGameState(){
        if(this.checkWinner() || this.checkFieldFull()){
            return true;
        }
    }

    //Some getters
    getStartingPlayer(){
        return this.currentStartingPlayer;
    }

    getField(){
        return this.gameField;
    }

    getWinner(){
        return this.winner;
    }

    getScore(){
        return this.score;
    }

    getGameType(){
        return this.gameType;
    }



    //---------------------------------------------BOTS------------------------------------------//

    /**
     * returns a list of all possible moves
     * @return list of numbers
     */
    getPossibleMoves(){
        let possibleMoves = []
        this.getField().forEach((cell, index) => {
            if(cell === ""){
                possibleMoves.push(index)
            }
        })
        return possibleMoves;
    }

    /**
     * Handles easy bot moves. Random moves
     */
    easyBotMove(){
        let possibleMoves = this.getPossibleMoves();
        return possibleMoves[Math.floor(Math.random() * possibleMoves.length)];
    }

    /**
     * handles normal bot moves, if no win/lose cell found, uses easy bot strategy
     */
    normalBotMove(){
        let winningMove = this.normalBotWinDefendMove(true);
        let defendingMove = this.normalBotWinDefendMove(false);
        if(winningMove !== null){
            return winningMove;
        }
        if(defendingMove !== null){
            return defendingMove;
        }
        return this.easyBotMove();
    }

    /**
     * Checks where normal bot should move
     * @param isForWin checks if we detect win condition or lose condition 
     * @return returns where next optimal move is. returns null if not win or lose condition found
     */
    normalBotWinDefendMove(isForWin){
        let possibleMoves = this.getPossibleMoves();
        let res = null;
        possibleMoves.forEach((cell) => {
            let copyOfField = this.gameField.slice();
            copyOfField[cell] = this.playerTwo;
            this.winningMoves.every((vector) => {
                if(isForWin){
                    if(vector.every((elem) => this.isPlayerTwoCell(elem,copyOfField))){
                        res = cell;
                        return false;
                    }
                }
                else{
                    let fakeEnemyWin = copyOfField.slice();
                    fakeEnemyWin[cell] = this.playerOne;
                    if(vector.every((elem) => this.isPlayerOneCell(elem,fakeEnemyWin))){
                        res = cell;
                        return false;
                    }
                }
              return true;
            })
        })
        return res;
    }
}

export default TicTacToeGame