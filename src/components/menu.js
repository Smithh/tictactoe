import React, { Component } from 'react';
import '../styles/Menu.css';

class Menu extends Component {
    
    render(){
        return(
            <div className="wrapper-class">
                <div className="menu">
                    <span className="span1 span" onClick={(event) => this.props.eventEmitter.emit("ActiveScreen",2,"Player_two")}>
                        Human_vs_Human
                    </span>
                    <span className="span2 span" onClick={(event) => this.props.eventEmitter.emit("ActiveScreen",2,"Easy_CPU")}>
                        Easy_Computer
                    </span>
                    <span className="span3 span" onClick={(event) => this.props.eventEmitter.emit("ActiveScreen",2,"Normal_CPU")}>
                        Normal_Computer
                    </span>
                </div>
            </div>
        );
    }
}

export default Menu;   