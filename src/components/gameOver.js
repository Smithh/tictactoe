import React, { Component } from 'react';
import '../styles/GameOver.css'

class GameOver extends Component{



    render(){
        return(
            <div className="game-over-wrapper">
                <div className="text-area">
                    <span className="winner-title">Winner:</span>
                    <span className="winner-name">{this.props.winner}</span>
                </div>
                <div className="new-game-menu">
                    <span className="menu-elem1 menu-elem" onClick={() => this.props.eventEmitter.emit("rematch")}>
                        Rematch
                    </span>
                    <span className="menu-elem2 menu-elem" onClick={() => this.props.eventEmitter.emit("MainMenu", 1)}>
                        Back_to_menu
                    </span>
                </div>
            </div>
        )
    }
}

export default GameOver;