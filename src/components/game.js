import React, { Component } from 'react';
import TicTacToeGame from '../TicTacToeGame';
import GameOver from './gameOver';
import ScoreBoard from './scoreBoard';
import '../styles/Game.css';

class Game extends Component{
    constructor(props){
        super(props);
        this.state = { 
            game : new TicTacToeGame(this.props.gameType,'X',[0,0,0]),
            field : ["","","","","","","","",""],
            gameStatus : true,
        }
    }

    componentWillMount(){
        this.eventEmitter = this.props.eventEmitter;        
    }
    


    updateField(){
        this.setState({field : this.state.game.getField()})
    }

    makeMove(Coord){
        if(this.state.gameStatus){
            this.state.game.addMove(Coord) ? this.setState({field: this.state.game.getField(), gameStatus : false }) : this.setState({field: this.state.game.getField()}); 
        }
    }

    render(){

        this.eventEmitter.on("rematch", () => {
            let nextStarter;
            this.state.game.getStartingPlayer() === "X" ? nextStarter = "O" : nextStarter = "X";
            this.setState({ game : new TicTacToeGame(this.props.gameType, nextStarter, this.state.game.getScore()), 
                            gameStatus : true},this.updateField);
        })

        let showEndComponent = <div></div>;
        if(!this.state.gameStatus){
            showEndComponent = <GameOver winner = {this.state.game.getWinner()} eventEmitter = {this.eventEmitter} />
        }
        return(
            <div className="wrapper-class">
                <div className="gameGrid">
                    {showEndComponent}
                    <div className="topLeft cell" onClick={() => { this.makeMove(0)}}>
                         {this.state.field[0]} 
                    </div>
                    <div className="topMiddle cell" onClick={() => this.makeMove(1)}> 
                        {this.state.field[1]} 
                    </div>
                    <div className="topRight cell" onClick={() => this.makeMove(2)}> 
                        {this.state.field[2]} 
                    </div>
                    <div className="midLeft cell" onClick={() => this.makeMove(3)}> 
                        {this.state.field[3]} 
                    </div>
                    <div className="midMiddle cell" onClick={() => this.makeMove(4)}> 
                        {this.state.field[4]} 
                    </div>
                    <div className="midRight cell" onClick={() => this.makeMove(5)}> 
                        {this.state.field[5]} 
                    </div>
                    <div className="bottomLeft cell" onClick={() => this.makeMove(6)}> 
                        {this.state.field[6]} 
                    </div>
                    <div className="bottomMiddle cell" onClick={() => this.makeMove(7)}> 
                        {this.state.field[7]} 
                    </div>
                    <div className="bottomRight cell" onClick={() => this.makeMove(8)}> 
                        {this.state.field[8]} 
                    </div>
                    <div className="score-board">
                        <ScoreBoard score = {this.state.game.getScore()} secondPlayer = {this.state.game.getGameType()}/>
                    </div>
                </div>
            </div>
        )
    }

}

export default Game;