import React, { Component } from 'react';
import '../styles/ScoreBoard.css';

class ScoreBoard extends Component{
    
    render(){
        return(
            <div className="score-board-wrapper">
                <div className="player-one score-element">
                    <span className="score-title">Player_One</span>
                    <span className="score">{this.props.score[0]}</span>
                </div>
                <div className="ties score-element">
                    <span className="score-title">Ties</span>
                    <span className="score">{this.props.score[2]}</span>
                </div>
                <div className="player-two score-element">
                    <span className="score-title">{this.props.secondPlayer}</span>
                    <span className="score">{this.props.score[1]}</span>
                </div>
            </div>
        )
    }
}

export default ScoreBoard;