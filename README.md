# README #

### TicTacToe Game ###

* Simple TicTacToe game made with React, HTML and Css

* Lowest screen size supported - 360x450
* IE & Edge dont support grid and known bug is scoreBoard.js elements are not placed properly


### How do I get set up? ###

* Clone project
* Install Node.js and npm
* Run "npm install" to install all dependencies
* Run "npm start"
* Open localhost:3000

### Who do I talk to? ###

* Mihkel Puusepp
