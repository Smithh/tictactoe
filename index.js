var express = require('express');
var app = express();

app.set('port', (process.env.PORT || 3000))

app.use(express.static(__dirname + '/build'))

app.get('*', function(request,response){
    response.sendFile(__dirname + '/build/index.html')
})

app.listen(process.env.PORT || 3000)